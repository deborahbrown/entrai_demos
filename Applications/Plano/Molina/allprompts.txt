Wave	Wording	Original wording
apology_re1c	My mistake.	My mistake.
apology_re1	Sorry.	Sorry.
apology_re2c	My mistake again.	My mistake again.
apology_re2	Sorry, I still didn't understand	Sorry, I still didn't understand
apology_to1	Sorry.	Sorry.
apology_to2	Sorry, I still didn't hear you.	Sorry, I still didn't hear you.
AR0007-1	We seem to be having problems. Let's get someone to help.	We seem to be having problems. Let's get someone to help.
AR0200_1	Hi. This is EntrAI the chatbot at UWF Bank.<br><br>I see you have an upcoming appointment with 	
AR0300_ini-1	This appointment is with	
AR0300_ini-2	Your new appointment with 	
AR0300_Jennifer	 Jennifer 	 Jennifer 
AR0300_Jose	 Jose 	 Jose 
AR0300_Juan	 Juan 	 Juan 
AR0300_Maria	 Maria 	 Maria 
AR0300_Michael	your financial advisor	
AR0400_DrBond	 Michael Bond	 
AR0400_DrHakimi	 with Dr. Hakimi 	 with Dr. Hakimi 
AR0400_DrMa	 wth Dr. Ma	 wth Dr. Ma
AR0400_DrRiaz	 with Dr. Riaz	 with Dr. Riaz
AR0400_DrSilva	 with Dr. Silva	 with Dr. Silva
AR0400_ini	The appointment is	The appointment is
AR0500_Fri1	tomorrow, Saturday	tomorrow, Saturday
AR0500_Fri2	 this coming Monday	 this coming Monday
AR0500_Mon1	tomorrow, Tuesday 	tomorrow, Tuesday 
AR0500_Mon2	 this Wednesday	 this Wednesday
AR0500_Thurs1	tomorrow, Friday	tomorrow, Friday
AR0500_Thurs2	 this Saturday	 this Saturday
AR0500_Tues1	tomorrow, Wednesday 	tomorrow, Wednesday 
AR0500_Tues2	 this Thursday	 this Thursday
AR0500_Wed1	tomorrow, Thursday	. tomorrow, Thursday
AR0500_Wed2	 this Friday.	 this Friday.
AR0550_Fri1	 Friday	 Friday
AR0550_Mon1	 Monday 	 Monday 
AR0550_Sat1	 Saturday	 Saturday
AR0550_Thurs1	 Thursday	 Thursday
AR0550_Tues1	 Tuesday	 Tuesday
AR0550_Wed1	 Wednesday	 Wednesday
AR0600_10	at 10:00AM	Ten a m
AR0600_1	at 1:00PM	One p m
AR0600_4	at 4:00PM	Four p m
AR0600_615	at 6:15PM	Six fifteen p m
AR0600_8	at 8:00AM	Eight a m
AR0600_915	at 9:15AM.	Nine fifteen a m
AR0600_9	at 9:00AM.	Nine a m
AR0650_1	The doctor's office is located at 	The doctor's office is located at.
AR0650_2	Sure, the address is	Sure, the address is
AR0650_3	You will be seen at 	You will be seen at.
AR0650_Burbank	500 East Olive Avenue, STE 315, in Burbank.	500 East Olive Avenue, Suite 315, in Burbank.
AR0650_NorthHills	15424 Nordhoff Street,  STE B, in North Hills.	15424 Nordhoff Street,  Suite B, in North Hills.
AR0650_Pasedena	10 Congress Street, STE 506, in Pasadena.	10 Congress Street, Suite 506, in Pasadena.
AR0650_ShermanOaks	115477 Ventura Blvd, STE 300, in Sherman Oaks.<br><br>
AR0650_VanNuys	7136 Haskell Avenue, STE 205, in Van Nuys.	7136 Haskell Avenue, Suite 205, in Van Nuys.
AR0700_Exit-1	Let's get someone to help you.	Let's get someone to help you.
AR0700_Exit-2	Let's get someone to help.	Let's get someone to help.
AR0700_Exit-3	Great. I will confirm that.	Great. I will confirm that.
AR0700_Exit-4	Okay, let's schedule another appointment for you.	Okay, let's schedule another appointment for you.
AR0700_Exit-5	Let's get someone to help.	Let's get someone to help.
AR0700_ini_First	Can you make that appointment?	Can you make that appointment?
AR0700_ini_Rep	Does this still work for you?	Does this still work for you?
AR0700_ini_Resched	Do you have that?	Do you have that?
AR0700__ni1_first	 	Can you make that appointment?
AR0700_ni1_Rep	Does this still work for you?	<Sorry.>  Does this still work for you?
AR0700__nm1_first	 	Can you make that appointment?
AR0700_nm1_Rep	Does this still work for you?	<Sorry.>  Does this still work for you?
AR0900_FastBlood	This appointment includes a fasting blood draw so don't eat anything for 8 hours before your appointment.  You are allowed black coffee or tea if you need caffeine to start your day. We look forward to seeing you. Good-bye.	This appointment includes a fasting blood draw so don't eat anything for 8 hours before your appointment.  You are allowed black coffee or tea if you need caffeine to start your day. We look forward to seeing you. Good-bye.
AR0900_NewPt	This is a new patient appointment. Please arrive one half hour early so your can fill out our registration forms.  We will also need a complete list of your current medications. If you want, you can print out these forms and fill them out ahead of time by going to www.acmehealthcare.com/ca/registration. If you do this, there is no need for you to arrive early.  We look forward to seeing you. Good-bye.	This is a new patient appointment. Please arrive one half hour early so your can fill out our registration forms.  We will also need a complete list of your current medications. If you want, you can print out these forms and fill them out ahead of time by going to www.acmehealthcare.com/ca/registration. If you do this, there is no need for you to arrive early.  We look forward to seeing you. Good-bye.
AR0900_None	We look forward to seeing you. Good-bye.	We look forward to seeing you. Good-bye.
AR0900_YrFirst	This is your first appointment with us this calendar year.  Please be sure to bring your 2017 member ID so we can update our records.  We look forward to seeing you. Good-bye.	This is your first appointment with us this calendar year.  Please be sure to bring your 2017 member ID so we can update our records.  We look forward to seeing you. Good-bye.
AR1000_10	10:00AM.	Ten a m
AR1000_1	1:00PM.	One p m
AR1000_4	4:00PM.	Four p m
AR1000_615	6:15PM.	Six fifteen p m
AR1000_8	8:00AM.	Eight a m
AR1000_915	9:15AM.	Nine fifteen a m
AR1000_9	9:00AM.	Nine a m
AR1000-Exit-1	Let's get someone to help.	Let's get someone to help.
AR1000-Exit-2	Great. Let's review this new appointment.	Great. Let's review this new appointment.
AR1000-Exit-3	Okay.	Okay.
AR1000_Fri	 Friday	 Friday
AR1000_ini-1a	Our next available appointment in that time frame is	Our next available appointment in that time frame is
AR1000_ini-1b	Our next available appointment is	Our next available appointment is.
AR1000_ini-2	 at 	 at 
AR1000_ini-3	Does this work for you?	Does this work for you?
AR1000_Mon	 Monday.	 Monday.
AR1000_ni1-1a	Our next available appointment in that time frame is	Our next available appointment in that time frame is
AR1000_ni1-1b	Our next available appointment is	Our next available appointment is
AR1000_ni1-3	Can you make this time?	Can you make this time?
AR1000_nm1-1a	Our next available appointment in that time frame is	Our next available appointment in that time frame is
AR1000_nm1-1b	Our next available appointment is	Our next available appointment is
AR1000_nm1-3	Can you make that appointment?	Can you make that appointment?
AR1000_Sat	 Saturday	 Saturday
AR1000_Thurs	 Thursday	 Thursday.
AR1000_Tues	 Tuesday	 Tuesday
AR1000_Wed	 Wednesday	 Wednesday.
AR1100_Confirm_10	Tuesday after work, right?	Tuesday after work, right?
AR1100_Confirm_11	That's Wednesday, right?	That's Wednesday, right?
AR1100_Confirm_12	Wednesday morning, right?	Wednesday morning, right?
AR1100_Confirm_13	Wednesday afternoon, right?	Wednesday afternoon, right?
AR1100_Confirm_14	Wednesday after school, right?	Wednesday after school, right?
AR1100_Confirm_15	Wednesday after work, right?	Wednesday after work, right?
AR1100_Confirm_16	That's Thursday, right?	That's Thursday, right?
AR1100_Confirm_17	Thursday morning, right?	Thursday morning, right?
AR1100_Confirm_18	Thursday afternoon, right?	Thursday afternoon, right?
AR1100_Confirm_19	Thursday after school, right?	Thursday after school, right?
AR1100_Confirm_1	That's Monday, right?	That's Monday, right?
AR1100_Confirm_20	Thursday after work, right?	Thursday after work, right?
AR1100_Confirm_21	That's Friday, right?	That's Friday, right?
AR1100_Confirm_22	Friday morning, right?	Friday morning, right?
AR1100_Confirm_23	Friday afternoon, right?	Friday afternoon, right?
AR1100_Confirm_24	Friday after school, right?	Friday after school, right?
AR1100_Confirm_25	Friday after work, right?	Friday after work, right?
AR1100_Confirm_26	That's Saturday, right?	That's Saturday, right?
AR1100_Confirm_27	Saturday morning, right?	Saturday morning, right?
AR1100_Confirm_28	Saturday afternoon, right?	Saturday afternoon, right?
AR1100_Confirm_29	That's Sunday, right?	That's Sunday, right?
AR1100_Confirm_2	Monday morning, right?	Monday morning, right?
AR1100_Confirm_30	Sunday mornings, right?	Sunday mornings, right?
AR1100_Confirm_31	Sunday afternoon, right?	Sunday afternoon, right?
AR1100_Confirm_3	Monday afternoon, right?	Monday afternoon, right?
AR1100_Confirm_4	Monday after school, right?	Monday after school, right?
AR1100_Confirm_5	Monday after work, right?	Monday after work, right?
AR1100_Confirm_6	That's Tuesday, right?	That's Tuesday, right?
AR1100_Confirm_7	Tuesday morning, right?	Tuesday morning, right?
AR1100_Confirm_8	Tuesday afternoon, right?	Tuesday afternoon, right?
AR1100_Confirm_9	Tuesday after school, right?	Tuesday after school, right?
AR1100_Exit-1	I'm sorry, the doctor's office is closed on Saturday afternoons.	I'm sorry, the doctor's office is closed on Saturday afternoons.
AR1100_Exit-2	I'm sorry, the doctor's office is closed on Sunday.	I'm sorry, the doctor's office is closed on Sunday.
AR1100_Exit-3	I'm sorry, the only night we are open late is on Thursdays.	I'm sorry, the only night we are open late is on Thursdays.
AR1100_ini	What day of the week works best for you?	What day of the week works best for you?
AR1100_ni1	<Sorry.>  What day of the week works best for you?	<Sorry.>  What day of the week works best for you?
AR1100_nm1	<Sorry.>  Does this still work for you?	<Sorry.>  Does this still work for you?
AR1200_1	I'm sorry, we close at noon on Saturdays.	I'm sorry, we close at noon on Saturdays.
AR1200_2	I'm sorry, the only night we are open late is on Thursdays.	I'm sorry, the only night we are open late is on Thursdays.
AR1200_Confirm_1	That's early morning before work, right?	That's early morning before work, right?
AR1200_Confirm_2	That's morning, right?	That's morning, right?
AR1200_Confirm_3	That's midday, right?	That's midday, right?
AR1200_Confirm_4	Early afternoon, right?	Early afternoon, right?
AR1200_Confirm_7	After school, right?	After school, right?
AR1200_Confirm_8	Late afternoon, right?	Late afternoon, right?
AR1200_Confirm_9	That's after work, right?	That's after work, right?
AR1200_ini	And what time of day?	And what time of day?
AR1200_ni1	What time of day fits your schedule?	<Sorry.>  What time of day fits your schedule?
AR1200_nm1	Does this still work for you?	<Sorry.>  Does this still work for you?
AR1300_ini	We are open Saturday morning from 8 am to noon. Should we try to book a Saturday appointment for you?	We are open Saturday morning from 8 am to noon. Should we try to book a Saturday appointment for you?
AR1300	Let's get you to someone who can help.	Let's get you to someone who can help.
AR1300_ni1	We are open Saturday morning from 8 am to noon. Should we try to book a Saturday appointment for you?	<Sorry.>  We are open Saturday morning from 8 am to noon. Should we try to book a Saturday appointment for you?
AR1300_nm1	We are open Saturday morning from 8 am to noon. Should we try to book a Saturday appointment for you?	<Sorry.>  We are open Saturday morning from 8 am to noon. Should we try to book a Saturday appointment for you?
Operator-exit	Alright, I'll get someone to help.	Alright, I'll get someone to help.
Sil-20ms	 	<20 ms silence>
Silence_25ms	 	
